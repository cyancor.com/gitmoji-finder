using System;
using Eto.Forms;
using Eto.Drawing;
using System.Collections.Generic;
using GitmojiFinder.Entities;
using System.Collections.ObjectModel;
using TextCopy;
using System.Runtime.InteropServices;
using Common.Entities;

namespace GitmojiFinder
{
    public partial class MainForm : Form
    {
        private SearchBox _searchBox;
        private GridView _gitmojiGrid;
        private bool _dialogIsOpen = false;

        private ObservableCollection<GitmojiItem> _gitmojiList = new ObservableCollection<GitmojiItem>();
        private GitmojiFinderService _gitmojiFinderService = new GitmojiFinderService();

        public MainForm()
        {
            _gitmojiList = _gitmojiFinderService.GitmojiList;

            Title = "Gitmoji Finder";
            MinimumSize = new Size(450, 400);
            ClientSize = new Size(450, 400);
            Icon = Icon.FromResource("GitmojiFinder.Assets.AppIcon.png");

            if (SettingsService.Instance.CurrentSettings.RememberWindowPosition)
            {
                var windowXFromStorage = SettingsService.Instance.CurrentSettings.WindowX;
                var windowYFromStorage = SettingsService.Instance.CurrentSettings.WindowY;

                if (windowXFromStorage != null && windowYFromStorage != null)
                {
                    Location = new Point((int)windowXFromStorage, (int)windowYFromStorage);
                }

                LocationChanged += WindowLocationChanged;
            }

            if (SettingsService.Instance.CurrentSettings.PressEscapeToQuit)
            {
                KeyDown += OnKeyDown;
            }

            Menu = new MenuBar();

            LostFocus += (sender, e) =>
            {
                if (!_dialogIsOpen)
                {
                    CloseApplicationWhen(SettingsService.Instance.CurrentSettings.CloseWhenLostFocus);
                }
            };

            Content = new TableLayout
            {
                Rows =
                {
                    SearchBar(),
                    Grid()
                }
            };

            GotFocus += (sender, e) =>
            {
                _searchBox.Focus();
            };
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Keys.Escape)
            {
                CloseApplicationWhen(SettingsService.Instance.CurrentSettings.PressEscapeToQuit);
            }
        }

        private void WindowLocationChanged(object sender, EventArgs e)
        {
            SettingsService.Instance.CurrentSettings.WindowX = Location.X;
            SettingsService.Instance.CurrentSettings.WindowY = Location.Y;
            SettingsService.Instance.Update();
        }

        private void CloseApplicationWhen(bool shouldClose)
        {
            if (shouldClose)
            {
                if (SettingsService.Instance.CurrentSettings.QuitInsteadOfClose && RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    Application.Instance.Quit();
                }
                else
                {
                    Close();
                }
            }
        }

        private GroupBox SearchBar()
        {
            _searchBox = new SearchBox
            {
                PlaceholderText = "Search"
            };
            _searchBox.KeyDown += (sender, e) =>
            {
                if (e.Key == Keys.Escape)
                {
                    CloseApplicationWhen(SettingsService.Instance.CurrentSettings.PressEscapeToQuit);
                }

                if (e.Key == Keys.Down)
                {
                    _gitmojiGrid.Focus();
                    _gitmojiGrid.SelectRow(0);
                }
            };

            _searchBox.TextChanged += (sender, e) =>
            {
                _gitmojiFinderService.Search(_searchBox.Text);
            };

            Icon settingsImage = Icon.FromResource("GitmojiFinder.Assets.SettingsIcon.png");
            

            var settingsButton = new Button
            {
                Width = 25,
                Height = 25,
                Image = settingsImage.WithSize(new Size(20, 20))
            };

            settingsButton.Click += SettingsButtonClicked;

            var control = new GroupBox
            {
                Padding = 5,
                Content = new TableLayout
                {
                    Spacing = new Size(5, 5),
                    Rows =
                    {
                        new TableRow
                        {
                            Cells =
                            {
                                new TableCell
                                {
                                    ScaleWidth = true,
                                    Control = _searchBox
                                },
                                new TableCell
                                {
                                    Control = settingsButton
                                }
                            }
                        }
                        
                    }
                }
            };

            return control;
        }

        private void SettingsButtonClicked(object sender, EventArgs e)
        {
            _dialogIsOpen = true;
            var dialog = new SettingsDialog
            {
                DisplayMode = DialogDisplayMode.Attached
            };
            dialog.ShowModal(this);
            _dialogIsOpen = false;
        }

        private GridView Grid()
        {
            _gitmojiGrid = new GridView();
            _gitmojiGrid.Columns.Add(new GridColumn { HeaderText = "Emoji", DataCell = new TextBoxCell("Emoji") });
            _gitmojiGrid.Columns.Add(new GridColumn { HeaderText = "Description", DataCell = new TextBoxCell("Description"), Expand = true });

            _gitmojiGrid.DataStore = _gitmojiList;

            _gitmojiGrid.KeyDown += (sender, e) =>
            {
                if (e.Key == Keys.Enter)
                {
                    if (((GridView)sender).SelectedRow >= 0)
                    {
                        string emoji = _gitmojiList[((GridView)sender).SelectedRow].Emoji;

                        if (emoji != null)
                        {
                            CopyToClipboard(emoji);

                            CloseApplicationWhen(SettingsService.Instance.CurrentSettings.CloseOnEnter);
                        }
                    }
                }

                if (e.Key == Keys.Up && _gitmojiGrid.SelectedRow <= 0)
                {
                    _searchBox.Focus();
                }
            };

            _gitmojiGrid.CellClick += (sender, e) =>
            {
                if (e.Row >= 0)
                {
                    CopyToClipboard(_gitmojiList[e.Row].Emoji);
                }
            };

            return _gitmojiGrid;
        }

        private void CopyToClipboard(string value)
        {
            try
            {
                try
                {
                    ClipboardService.SetText(value);
                }
                catch (Exception)
                {
                    new Eto.Forms.Clipboard { Text = value };
                }

                if (SettingsService.Instance.CurrentSettings.NotificationOnCopy)
                {
                    var notification = new Notification
                    {
                        Title = "Copied to clipboard",
                        Message = value
                    };

                    notification.Show();
                }

                CloseApplicationWhen(SettingsService.Instance.CurrentSettings.CloseOnCopy);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}
