﻿using System;
namespace GitmojiFinder.Entities
{
    public class Config
    {
        public bool CloseWhenLostFocus { get; set; } = false;
        public bool CloseOnCopy { get; set; } = false;
        public bool CloseOnEnter { get; set; } = false;
        public bool QuitInsteadOfClose { get; set; } = false;
        public bool NotificationOnCopy { get; set; } = true;
        public bool RememberWindowPosition { get; set; } = true;
        public bool PressEscapeToQuit { get; set; } = true;
        public int? WindowX { get; set; }
        public int? WindowY { get; set; }
    }
}
