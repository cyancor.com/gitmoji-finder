﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;
using GitmojiFinder.Entities;
using System.Runtime.InteropServices;

namespace GitmojiFinder
{
    public class SettingsDialog : Dialog
    {
        private Config _currentSettings = SettingsService.Instance.CurrentSettings;

        public SettingsDialog()
        {
            Title = "Settings";

            MinimumSize = new Size(350, 300);
            ClientSize = new Size(350, 300);

            Content = new TableLayout
            {
                Padding = 10,
                Rows = { CheckBoxes() }
            };


            // buttons
            DefaultButton = new Button { Text = "Done" };
            DefaultButton.Click += (sender, e) => Close();
            PositiveButtons.Add(DefaultButton);
        }

        private Control CheckBoxes()
        {
            var stackLayout = new StackLayout();

            var groupBox = new GroupBox
            {
                Padding = 10,
                Content = stackLayout
            };


            // Close/Quit when losing focus
            var closeWhenLostFocusCheckbox = new CheckBox { Checked = _currentSettings.CloseWhenLostFocus, Text = "Close window when losing focus" };
            closeWhenLostFocusCheckbox.CheckedChanged += (sender, e) =>
            {
                _currentSettings.CloseWhenLostFocus = closeWhenLostFocusCheckbox.Checked ?? _currentSettings.CloseWhenLostFocus;
                SettingsService.Instance.Update();
            };

            stackLayout.Items.Add(closeWhenLostFocusCheckbox);


            // Close/Quit on Enter
            var closeOnEnterCheckbox = new CheckBox { Checked = _currentSettings.CloseOnEnter, Text = "Close window on Enter" };
            closeOnEnterCheckbox.CheckedChanged += (sender, e) =>
            {
                _currentSettings.CloseOnEnter = closeOnEnterCheckbox.Checked ?? _currentSettings.CloseOnEnter;
                SettingsService.Instance.Update();
            };

            stackLayout.Items.Add(closeOnEnterCheckbox);


            // Show notification on copy
            var notificationOnCopyCheckbox = new CheckBox { Checked = _currentSettings.NotificationOnCopy, Text = "Show notification on copy" };
            notificationOnCopyCheckbox.CheckedChanged += (sender, e) =>
            {
                _currentSettings.NotificationOnCopy = notificationOnCopyCheckbox.Checked ?? _currentSettings.NotificationOnCopy;
                SettingsService.Instance.Update();
            };
            stackLayout.Items.Add(notificationOnCopyCheckbox);


            // Remember window position
            var rememberWindowPositionCheckbox = new CheckBox { Checked = _currentSettings.RememberWindowPosition, Text = "Remember window position" };
            rememberWindowPositionCheckbox.CheckedChanged += (sender, e) =>
            {
                _currentSettings.RememberWindowPosition = rememberWindowPositionCheckbox.Checked ?? _currentSettings.RememberWindowPosition;
                SettingsService.Instance.Update();
            };
            stackLayout.Items.Add(rememberWindowPositionCheckbox);


            // Press esc to exit
            var pressEscapeToQuitCheckbox = new CheckBox { Checked = _currentSettings.PressEscapeToQuit, Text = "Press esc to close" };
            pressEscapeToQuitCheckbox.CheckedChanged += (sender, e) =>
            {
                _currentSettings.PressEscapeToQuit = pressEscapeToQuitCheckbox.Checked ?? _currentSettings.PressEscapeToQuit;
                SettingsService.Instance.Update();
            };
            stackLayout.Items.Add(pressEscapeToQuitCheckbox);


            // Exit on copy
            var closeOnCopyCheckbox = new CheckBox { Checked = _currentSettings.CloseOnCopy, Text = "Close on copy" };
            closeOnCopyCheckbox.CheckedChanged += (sender, e) =>
            {
                _currentSettings.CloseOnCopy = closeOnCopyCheckbox.Checked ?? _currentSettings.CloseOnCopy;
                SettingsService.Instance.Update();
            };
            stackLayout.Items.Add(closeOnCopyCheckbox);


            if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                stackLayout.Items.Add(new Label());
                stackLayout.Items.Add(new Label {
                    Text = "macOS",
                    TextColor = Color.FromGrayscale(0.5f)
                });

                var quitInsteadOfCloseCheckbox = new CheckBox { Checked = _currentSettings.QuitInsteadOfClose, Text = "Quit instead of close window" };
                quitInsteadOfCloseCheckbox.CheckedChanged += (sender, e) =>
                {
                    _currentSettings.QuitInsteadOfClose = quitInsteadOfCloseCheckbox.Checked ?? _currentSettings.QuitInsteadOfClose;
                    SettingsService.Instance.Update();
                };

                stackLayout.Items.Add(quitInsteadOfCloseCheckbox);
            }


            return groupBox;
        }
    }
}
