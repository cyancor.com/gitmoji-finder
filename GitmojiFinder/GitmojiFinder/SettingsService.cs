﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.Json;
using Eto.Forms;
using GitmojiFinder.Entities;

namespace GitmojiFinder
{
    public class SettingsService
    {
        private readonly string _macOSApplicationPath = "Library/Application Support";
        private readonly string _applicationName = "GitmojiFinder";
        private readonly string _configFileName = "Config.json";

        private static SettingsService _instance;

        public Config CurrentSettings { get; set; }

        public static SettingsService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SettingsService();
                }

                return _instance;
            }
        }


        public SettingsService()
        {
            CurrentSettings = GetConfig();
        }

        private string GetConfigPath()
        {
            try
            {
                string path;

                if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    path = $"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}/{_macOSApplicationPath}/{_applicationName}";
                }
                else
                {
                    path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), _applicationName);
                }

                DirectoryInfo directoryInfo = Directory.CreateDirectory(path);

                string filePath = Path.Combine(path, _configFileName);

                return filePath;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
                throw exception;
            }
        }

        private void WriteConfigToFile(Config config)
        {
            try
            {
                string configSerialized = JsonSerializer.Serialize(config, new JsonSerializerOptions { WriteIndented = true });
                File.WriteAllText(GetConfigPath(), configSerialized);
            }
            catch(Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }
        }

        private Config GetConfig()
        {
            try
            {
                string filePath = GetConfigPath();

                bool fileExists = File.Exists(filePath);

                var currentConfig = new Config();

                if (fileExists)
                {
                    string rawFile = File.ReadAllText(filePath);
                    Config configFile = JsonSerializer.Deserialize<Config>(rawFile);

                    currentConfig = configFile;
                }
                else
                {
                    WriteConfigToFile(currentConfig);
                }

                return currentConfig;
            }
            catch(Exception exception)
            {
                MessageBox.Show(exception.ToString());

                return new Config();
            }
        }

        public void Update()
        {
            WriteConfigToFile(CurrentSettings);
        }
      
    }
}
