<div align="center">
<p align="center">
<img align="center" src="https://gitlab.com/cyancor.com/gitmoji-finder/-/raw/develop/Design/GnomeScreenshot.png" width="300"/>
</p>
</div>


<div align="center">
<h1 style="text-align: center">Gitmoji Finder</h1>
</div>
<a href="https://gitmoji.dev">
  <img src="https://img.shields.io/badge/gitmoji-%20😜-FFDD67.svg?style=flat-square" alt="Gitmoji">
</a>

A simple program that helps you to find the correct emoji for your commit-message. <br />
Runs on Linux, macOS and Windows
<br />
<br />

## Installation
Download the executable for your OS from the release page. <br />
- Linux: GitmojiFinder
- macOS: GitmojiFinder.app
- Windows: GitmofiFinder.exe

<br />
<br />

## Usage on Linux

`chmod +x GitmofiFinder` to make the file executable.

`./GitmojiFinder` to execute