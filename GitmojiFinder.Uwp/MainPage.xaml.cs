﻿using Common.Entities;
using GitmojiFinder.Uwp.Entities;
using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Core;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace GitmojiFinder.Uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private readonly GitmojiFinderService _gitmojiFinderService;
        private readonly Config _currentConfig;
        private string _searchQuery;

        private ObservableCollection<GitmojiItem> Gitmojis { get; set; }
        private string SearchQuery { get => _searchQuery; set => Search(value); }

        public MainPage()
        {
            _currentConfig = SettingsService.Instance.CurrentConfig;
            _gitmojiFinderService = new GitmojiFinderService();
            Gitmojis = _gitmojiFinderService.GitmojiList;
            DataContext = this;
            this.InitializeComponent();

            Window.Current.Activated += OnWindowActivated;
            KeyDown += MainPageOnKeyDown;
        }

        private void MainPageOnKeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Escape)
            {
                if (_currentConfig.PressEscapeToClose)
                {
                    CloseApplication();
                }
            }
        }

        private async void CloseApplication()
        {
            await ApplicationView.GetForCurrentView().TryConsolidateAsync();
        }

        private void OnWindowActivated(object sender, Windows.UI.Core.WindowActivatedEventArgs e)
        {
            if (e.WindowActivationState == Windows.UI.Core.CoreWindowActivationState.Deactivated)
            {
                if (_currentConfig.CloseWhenLoosingFocus)
                {
                    CloseApplication();
                }
            }
            else
            {
                SearchBoxElement.Focus(FocusState.Programmatic);
            }
        }

        private void Search(string value)
        {
            _searchQuery = value;
            _gitmojiFinderService.Search(value);
        }

        private void SearchTextChanged(object sender, TextChangedEventArgs e)
        {
            var query = ((TextBox)sender).Text;
            _gitmojiFinderService.Search(query);
        }

        private void SearchBoxKeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Down)
            {
                GitmojiListView.Focus(FocusState.Programmatic);
                GitmojiListView.SelectedIndex = 0;
            }
        }

        private void CopyEmojiToClipboard(object sender, DoubleTappedRoutedEventArgs e)
        {
            var selectedItem = ((ListView)sender).SelectedItem as GitmojiItem;

            if (selectedItem == null)
            {
                return;
            }

            var dataPackage = new DataPackage { RequestedOperation = DataPackageOperation.Copy };
            dataPackage.SetText(selectedItem.Emoji);
            Clipboard.SetContent(dataPackage);

            if (_currentConfig.NotificationOnCopy)
            {
                var toastBuilder = new ToastContentBuilder()
                    .AddText(selectedItem.Emoji + " Copied to clipboard")
                    .SetToastDuration(ToastDuration.Short);

                toastBuilder.Show();
            }

            if (_currentConfig.CloseOnCopy)
            {
                CloseApplication();
            }
        }

        private void GitmojiListViewKeyDown(object sender, KeyRoutedEventArgs e)
        {
            var selectedIndex = ((ListView)sender).SelectedIndex;

            if (e.Key == Windows.System.VirtualKey.Up && selectedIndex <= 0)
            {
                SearchBoxElement.Focus(FocusState.Programmatic);
            }

            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                CopyEmojiToClipboard(sender, null);
            }
        }

        private async void SettingsButtonClicked(object sender, RoutedEventArgs e)
        {
            var settingsDialog = new SettingsDialog();
            await settingsDialog.ShowAsync();
        }
    }
}
