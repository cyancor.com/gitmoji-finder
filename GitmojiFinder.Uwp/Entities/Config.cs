﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitmojiFinder.Uwp.Entities
{
    internal class Config : Notifiable
    {
        private bool _closeWhenLoosingFocus = false;
        private bool _closeOnCopy = false;
        private bool _pressEscapeToClose = true;
        private bool _notificationOnCopy = true;

        public bool CloseWhenLoosingFocus { get => _closeWhenLoosingFocus; set => Set(ref _closeWhenLoosingFocus, value); }
        public bool CloseOnCopy { get => _closeOnCopy; set => Set(ref _closeOnCopy, value); }
        public bool PressEscapeToClose { get => _pressEscapeToClose; set => Set(ref _pressEscapeToClose, value); }
        public bool NotificationOnCopy { get => _notificationOnCopy; set => Set(ref _notificationOnCopy, value); }
    }
}
