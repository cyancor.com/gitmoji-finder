﻿using GitmojiFinder.Uwp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitmojiFinder.Uwp
{
    internal class SettingsService
    {
        private static SettingsService _instance;

        public static SettingsService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SettingsService();
                }

                return _instance;
            }
        }


        public Config CurrentConfig { get; set; } = new Config();

        public SettingsService()
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            var closeOnCopy = localSettings.Values[nameof(CurrentConfig.CloseOnCopy)] as bool?;
            var closeWhenLoosingFocus = localSettings.Values[nameof(CurrentConfig.CloseWhenLoosingFocus)] as bool?;
            var pressEscapeToClose = localSettings.Values[nameof(CurrentConfig.PressEscapeToClose)] as bool?;
            var notificationOnCopy = localSettings.Values[nameof(CurrentConfig.NotificationOnCopy)] as bool?;

            CurrentConfig.CloseOnCopy = closeOnCopy ?? CurrentConfig.CloseOnCopy;
            CurrentConfig.CloseWhenLoosingFocus = closeWhenLoosingFocus ?? CurrentConfig.CloseWhenLoosingFocus;
            CurrentConfig.PressEscapeToClose = pressEscapeToClose ?? CurrentConfig.PressEscapeToClose;
            CurrentConfig.NotificationOnCopy = notificationOnCopy ?? CurrentConfig.NotificationOnCopy;

            CurrentConfig.PropertyChanged += CurrentConfigPropertyChanged;
        }

        private void CurrentConfigPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            try
            {
                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                var newValue = sender.GetType().GetProperty(e.PropertyName).GetValue(sender, null);
                localSettings.Values[e.PropertyName] = newValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
