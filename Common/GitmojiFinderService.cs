﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Text.Json;

namespace GitmojiFinder
{
    public class GitmojiFinderService
    {
        private List<GitmojiItem> _fullList = new List<GitmojiItem>();
        public ObservableCollection<GitmojiItem> GitmojiList { get; set; } = new ObservableCollection<GitmojiItem>();

        public GitmojiFinderService()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "Common.Entities.GitmojiList.json";

            using Stream stream = assembly.GetManifestResourceStream(resourceName);
            using StreamReader reader = new StreamReader(stream);

            string json = reader.ReadToEnd();
            GitmojiItem[] items = JsonSerializer.Deserialize<GitmojiItem[]>(json);

            _fullList = new List<GitmojiItem>(items);
            GitmojiList = new ObservableCollection<GitmojiItem>(items);
        }

        public void Search(string query)
        {
            query = query.Trim();

            if (query.Length <= 0)
            {
                GitmojiList.Clear();

                foreach (var item in _fullList)
                {
                    GitmojiList.Add(item);
                }

                return;
            }

            var items = _fullList.FindAll(
                item => item.Tags.ToLower().Contains(query.ToLower()) ||
                item.Description.ToLower().Contains(query.ToLower()) ||
                item.Alias.ToLower().Contains(query.ToLower()) ||
                item.Emoji.ToLower().Contains(query.ToLower())
            );

            GitmojiList.Clear();

            foreach (var item in items)
            {
                GitmojiList.Add(item);
            }
        }
    }
}
