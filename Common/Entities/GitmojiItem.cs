﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Common.Entities
{
    public class GitmojiItem
    {
        [JsonPropertyName("emoji")]
        public string Emoji { get; set; }


        [JsonPropertyName("alias")]
        public string Alias { get; set; }


        [JsonPropertyName("description")]
        public string Description { get; set; }


        [JsonPropertyName("tags")]
        public string Tags { get; set; }
    }
}
